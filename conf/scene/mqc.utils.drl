package mqc

import java.util.List
import java.util.ArrayList
import models.context.*
import models.context.utils.Value
import java.time.Duration
import java.time.LocalDateTime
import java.time.Instant
import java.util.TimeZone
import akka.event.LoggingAdapter

function double earthRadius() {
    return (6.37814) * Math.pow(10, 6);
}

function double degreesToRadians (double degrees) {
    return degrees * Math.PI / 180;
}

function double distance(ContextValue x, ContextValue y) {
    double latX = x.getEntries().get("latitude").asNumber();
    double lngX = x.getEntries().get("longitude").asNumber();

    double latY = y.getEntries().get("latitude").asNumber();
    double lngY = y.getEntries().get("longitude").asNumber();

    double dlat = degreesToRadians(latX - latY);
    double dlng = degreesToRadians(lngX - lngY);
    latX = degreesToRadians(latX);
    latY = degreesToRadians(latY);
    double a = Math.sin(dlat/2) * Math.sin(dlat/2) + Math.sin(dlng/2) * Math.sin(dlng/2) * Math.cos(latX) * Math.cos(latY);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return earthRadius() * c;
}


function double speed(List positions) {
    if (positions.size() < 2) {
        return 6.0;
    } else {
        ArrayList<Double> speeds = new ArrayList<>();
        for(int i = 0; i < positions.size() - 1; i++) {
            ContextValue curr = (ContextValue) positions.get(i);
            ContextValue next = (ContextValue) positions.get(i + 1);
            double ds = distance(curr, next);
            long dt = (next.getTimestamp() - curr.getTimestamp()) / 1000;
            speeds.add(ds/dt);
        }
        Double avg = speeds.stream().reduce(0.0, (a, b) -> a + b) / speeds.size();
        double speed = Math.round(avg*100.0)/100.0;
        if (speed > Double.MAX_VALUE) return Double.MAX_VALUE;
        if (speed < 6.0) return 6.0;
        return speed;
    }
}

function long estimateTimeToThreshold(List temperatures, long now, double maxTemperature) {
        if (temperatures.size() < 2) {
           return Long.MAX_VALUE;
        } else {
            ContextValue temp1 = (ContextValue) temperatures.get(temperatures.size() - 2);
            ContextValue temp2 = (ContextValue) temperatures.get(temperatures.size() - 1);
            double m = (temp2.getEntries().get("value").asNumber()  - temp1.getEntries().get("value").asNumber()) / (temp2.getTimestamp() - temp1.getTimestamp());
            long maxTime = (long) ((maxTemperature - temp2.getEntries().get("value").asNumber()) / m) + temp2.getTimestamp();
            long ttt = Duration.between(LocalDateTime.ofInstant(Instant.ofEpochMilli(now), TimeZone.getDefault().toZoneId()),
                                        LocalDateTime.ofInstant(Instant.ofEpochMilli(maxTime), TimeZone.getDefault().toZoneId())).getSeconds();

            return ttt;
        }
}


function double estimateTimeOfArrival(double speed, ContextValue locationA, ContextValue locationB) {
        double distance = distance(locationA, locationB);
                if (distance > Long.MAX_VALUE) return  Long.MAX_VALUE;
                if (distance < 500) return 0 + 200;
                return (long) (distance / speed) + 200;
}