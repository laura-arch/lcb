package repositories;

import br.ufes.inf.lprm.situation.model.Participation;
import models.context.Entity;
import models.situation.PersistentSituation;

import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class SituationRepository {

    private Map<Long, PersistentSituation> situations = new HashMap<Long, PersistentSituation>();
    private Map<Long, PersistentSituation> ongoing = new HashMap<Long, PersistentSituation>();

    public PersistentSituation save(br.ufes.inf.lprm.situation.model.Situation sit) {
        PersistentSituation situation = new PersistentSituation((long) (situations.size() + 1), sit );
        situations.put(situation.getId(), situation);
        ongoing.put(situation.getSituation().getUID(), situation);
        return situation;
    }

    public Optional<PersistentSituation> getById(long id) {
        return Optional.ofNullable(situations.get(id));
    }

    public Optional<PersistentSituation> getByOngoingId(long oid) {
        return Optional.ofNullable(ongoing.get(oid));
    }

    public void removeOngoing(long oid) {
        ongoing.remove(oid);
    }

    public List<PersistentSituation> getAll() {
        return new ArrayList<>(situations.values());
    }

    private boolean isOrContains(Object o, Long id) {
        if (Collection.class.isAssignableFrom(o.getClass())) {
            Collection<Entity> x = (Collection) o;
            return x.stream().filter(e -> e.getId().equals(id)).collect(Collectors.toSet()).isEmpty();
        } else if (o instanceof Entity) {
            return ((Entity) o).getId().equals(id);
        } else return false;
    }

    public List<PersistentSituation> filterBy(String type, Boolean active, Map<String, Long> participants) {
        Stream<PersistentSituation> stream = situations.values().stream();
        if (type != null) stream = stream.filter(s -> s.getSituation().getType().getName().equals(type));
        if (active != null) stream = stream.filter(s -> s.getSituation().isActive() == active);
        if (participants != null) {
            for (String key: participants.keySet()) {
                stream = stream.filter(s -> !s.getSituation()
                                             .getParticipations()
                                             .stream()
                                             .filter(p -> p.getPart().getLabel().equals(key) && isOrContains(p.getActor(), participants.get(key)))
                                                          .collect(Collectors.toSet()).isEmpty()
                                    );
            }
        }
        return stream.collect(Collectors.toList());
    }

}
