package modules;

import actors.simulators.ContainerTemperatureSimulator;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import models.context.*;
import models.context.utils.Value;
import services.ContextBrokerService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StartupModule {

    private final ContextBrokerService contextBrokerService;
    private final ActorSystem actorSystem;
    private ActorRef simulator;

    @Inject
    StartupModule(ContextBrokerService contextBrokerService, ActorSystem actorSystem) {
        this.contextBrokerService = contextBrokerService;
        this.actorSystem = actorSystem;
        /*Entity mary = new Entity("Person").setDescriptor("Dr. Mary Sue");
        mary.getAttributes().put("age", new Value(35));
        contextBrokerService.registerEntity(mary);
        IntrinsicContext temperature = new IntrinsicContext(mary);
        temperature.setKind("Temperature");
        temperature.setDescriptor("Dr. Mary Sue's temperature");
        mary.getContexts().put(temperature.getKind(), temperature);
        contextBrokerService.registerIntrinsicContext(temperature);*/
        setupMQC();
    }

    public void setupMQC() {
        Entity mary = new Entity("Person")
                            .setDescriptor("Dr. Mary Sue")
                            .addAttribute("email", new Value("marysue@laura.org"));

        Context maryGeo = new IntrinsicContext(mary)
                            .setKind("Geolocation")
                            .setDescriptor("Mary's smartphone GPS");



        Context speed = new IntrinsicContext(mary)
                .setKind("Speed")
                .setDescriptor("Mary's speed");

        contextBrokerService.registerEntity(mary);
        contextBrokerService.registerIntrinsicContext((IntrinsicContext) maryGeo);
        contextBrokerService.registerIntrinsicContext((IntrinsicContext) speed);
        mary.getContexts().put(maryGeo.getKind(), maryGeo);
        mary.getContexts().put(speed.getKind(), speed);

        ContextValue position = new ContextValue(maryGeo, System.currentTimeMillis());
        position.getEntries().put("latitude", new Value(-20.2990012));
        position.getEntries().put("longitude", new Value(40.2939304));

        contextBrokerService.registerContextValue(maryGeo, position);

        Entity container = new Entity("Container").setDescriptor("Freezer @office");
        Context temperature = new IntrinsicContext(container)
                .setKind("Temperature")
                .setDescriptor("Freezer's temperature");
        Context containerGeo = new IntrinsicContext(container)
                .setKind("Geolocation")
                .setDescriptor("Container's GPS");


        contextBrokerService.registerEntity(container);
        contextBrokerService.registerIntrinsicContext((IntrinsicContext) temperature);
        contextBrokerService.registerIntrinsicContext((IntrinsicContext) containerGeo);

        container.getContexts().put(temperature.getKind(), temperature);
        container.getContexts().put(containerGeo.getKind(), containerGeo);

        position = new ContextValue(containerGeo, System.currentTimeMillis());
        position.getEntries().put("latitude", new Value(-20.2990012));
        position.getEntries().put("longitude", new Value(40.2939304));
        contextBrokerService.registerContextValue(containerGeo, position);

        Entity botox = new Entity("Product")
                            .setDescriptor("Botox 00553211")
                            .addAttribute("maxTemperature", new Value(-5));
        contextBrokerService.registerEntity(botox);

        RelationalContext containment = new RelationalContext("Containment", "00553211 @ Freezer")
                .addPart("container", container)
                .addPart("contained", botox);

        RelationalContext observation = new RelationalContext("Observation", "Mary is observing 00553211")
                .addPart("observer", mary)
                .addPart("observed", botox);

        contextBrokerService.registerContext(containment);
        contextBrokerService.registerContext(observation);

        //simulator = actorSystem.actorOf(ContainerTemperatureSimulator.getProps((IntrinsicContext) temperature, contextBrokerService));
    }

}
