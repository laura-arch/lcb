package actors.simulators;

import akka.actor.AbstractActor;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.actor.Scheduler;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import models.context.ContextValue;
import models.context.IntrinsicContext;
import models.context.utils.Value;
import services.ContextBrokerService;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class ContainerTemperatureSimulator extends AbstractActor {

    private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);

    private final Scheduler scheduler;
    private final IntrinsicContext context;
    private final ContextBrokerService brokerService;
    private Cancellable observationTask;
    private long counter = 0;

    private boolean ascending;

    public static Props getProps(IntrinsicContext context, ContextBrokerService contextBrokerService) {
        return Props.create(ContainerTemperatureSimulator.class, context, contextBrokerService);
    }

    public ContainerTemperatureSimulator(IntrinsicContext context, ContextBrokerService contextBrokerService) {
        this.scheduler = getContext().getSystem().getScheduler();
        this.context = context;
        this.brokerService = contextBrokerService;
        this.ascending = true;
    }

    private Cancellable publishNewObservation(Map<String, Value> value, long delay) {
        ContextValue contextVal = new ContextValue(context, System.currentTimeMillis() + delay);
        contextVal.setEntries(value);
        return scheduler.scheduleOnce(Duration.ofMillis(delay), self(), contextVal,
                getContext().getSystem().dispatcher(), self());
    }

    private Value newValue() {
        double minTemp = -10;
        double maxTemp = -5;
        if (context.getValue() != null) {
            double current = context.getValue().getEntries().get("value").asNumber();
            Value val = new Value(ascending ? current + 0.5 : current - 0.5);
            if (current <= minTemp) ascending = true;
            if (current >= maxTemp) ascending = false;
            return val;
        } else return new Value((minTemp + maxTemp)/2);
    }

    @Override
    public void preStart() {
        logger.info("starting simulator for context '{}' from entity '{}'", context.getKind(), context.getBearer().getDescriptor());
        Map<String, Value> values = new HashMap<>();
        values.put("value", newValue());
        observationTask = publishNewObservation(values, 1000);
    }

    @Override
    public void postStop() {
        observationTask.cancel();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ContextValue.class, message -> {
                    logger.info("simulator for context '{}' from entity '{}' publishing new value", context.getKind(), context.getBearer().getDescriptor());
                    brokerService.registerContextValue(context, message);
                    Map<String, Value> values = new HashMap<>();
                    values.put("value", newValue());
                    if (counter == 10) {
                        publishNewObservation(values, 150000);
                        counter = 0;
                    } else publishNewObservation(values, 30000);
                    counter++;
                }
                ).build();
    }
}
