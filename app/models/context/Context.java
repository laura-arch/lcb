package models.context;

import models.Model;

public abstract class Context extends Model {

    private ContextType type;
    private String descriptor;
    private String kind;
    private ContextValue value;

    public String getDescriptor() {
        return descriptor;
    }

    public Context setDescriptor(String descriptor) {
        this.descriptor = descriptor;
        return this;
    }

    public ContextValue getValue() {
        return value;
    }

    public Context setValue(ContextValue value) {
        this.value = value;
        return this;
    }

    public String getKind() {
        return kind;
    }

    public Context setKind(String kind) {
        this.kind = kind;
        return this;
    }

    public ContextType getType() {
        return type;
    }

    public Context setType(ContextType type) {
        this.type = type;
        return this;
    }
}
