package models.context;

import br.ufes.inf.lprm.scene.util.SituationCast;

import java.util.HashMap;
import java.util.Map;

public class RelationalContext extends Context {

    private int hash = 17;
    private Map<String, Entity> parts = new HashMap<>();

    public RelationalContext() {
        this.setType(ContextType.relational);
    }

    public RelationalContext(String kind, String descriptor) {
        this();
        hash = this.hash + kind.hashCode();
        setKind(kind);
        setDescriptor(descriptor);
    }

    public RelationalContext(Long cid) {
        this.id = cid;
    }

    public Map<String, Entity> getParts() {
        return parts;
    }

    public RelationalContext addPart(String label, Entity entity) {
        this.hash = 31*this.hash + label.hashCode();
        this.hash = 31*this.hash + entity.hashCode();
        getParts().put(label, entity);
        return this;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        else {
            if ( !(obj instanceof RelationalContext) ) {
                return false;
            }
            else return this.hashCode() == obj.hashCode();
        }
    }

    @Override
    public int hashCode() {
        return hash;
    }

}
