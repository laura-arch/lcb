package models.context;

import models.Model;
import models.context.utils.Value;
import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;

import java.util.HashMap;
import java.util.Map;

@Role(Role.Type.EVENT)
@Timestamp("timestamp")
public class ContextValue extends Model {

    private Long timestamp;
    private Map<String, Value> entries = new HashMap<>();
    private Context owner;

    public ContextValue() {}

    public ContextValue (Context owner, Long timestamp) {
        this.owner = owner;
        this.timestamp = timestamp;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Value> getEntries() {
        return entries;
    }

    public void setEntries(Map<String, Value> entries) {
        this.entries = entries;
    }

    public Context getOwner() {
        return owner;
    }

    public void setOwner(Context owner) {
        this.owner = owner;
    }

    public ContextValue addEntry(String entry, Value value) {
        getEntries().put(entry, value);
        return this;
    }

}
